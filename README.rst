=====
Lavinder
=====

A small yet productive desktop environment. Forked from lavinder.

Status: in development.


.. image:: https://badges.gitter.im/LavinderDE/community.svg
   :alt: Join the chat at https://gitter.im/LavinderDE/community
   :target: https://gitter.im/LavinderDE/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge